import bm from 'box-muller'
import cc from 'color-convert'
import loadImage from 'image-promise'

const initialBase = {h: -14.25, s: 0.52, l: 0.28}
const limit = (value, min, max) => Math.min(Math.max(value, min), max)
const nd = (mean, sd) => sd * bm() + mean
const transformLinearInputToParabola = (value, srcMax, destMax, degree=2) => {
  const coef = destMax / (srcMax ** degree)
  return value ** degree * coef
}
const applyNormalDistribution = ({ h, s, l }, { h: hsd, s: ssd, l: lsd }) => {
  hsd = transformLinearInputToParabola(hsd, 360, 180, 2)
  ssd = transformLinearInputToParabola(ssd, 100, 1, 2)
  lsd = transformLinearInputToParabola(lsd, 100, 1, 3)
  return {
    h: (360 + nd(h, hsd)) % 360,
    s: limit(nd(s, ssd), 0, 1),
    l: limit(nd(l, lsd), 0, 1)
  }
}
const createMatrix = ({ baseMatrix, length, height, width, standardDeviations }) =>
  Array(length).fill().map(() =>
    baseMatrix.map(line =>
      line.map(base =>
        applyNormalDistribution(base, standardDeviations))))
const createBaseMatrix = ({ base, height, width }) =>
  Array(height).fill().map(() =>
    Array(width).fill().map(() => base))

const initialState = {
  imageUrl: null,
  base: initialBase,
  baseMatrix: [],
  standardDeviations: {h: 121, s: 54, l: 47},
  length: 4,
  height: 4,
  width: 4,
  matrix: []
}
initialState.baseMatrix = createBaseMatrix(initialState)
initialState.matrix = createMatrix(initialState)
export {initialState}

export const refresh = () => state => {
  return {...state, matrix: createMatrix(state)}
}

export const refreshBaseMatrix = () => state => (dispatch, _getState) => {
  const { width, height, imageUrl } = state
  if (imageUrl) {
    loadImage(imageUrl).then(image => {
      const canvas = document.createElement('canvas')
      const ctx = canvas.getContext('2d')
      ctx.drawImage(image, 0, 0, width, height)
      const { data } = ctx.getImageData(0, 0, width, height)
      let i = 0
      const baseMatrix = Array(height).fill().map(() =>
        Array(width).fill().map(() => {
          const [ h, s, l ] = cc.rgb.hsl([data[i++], data[i++], data[i++]])
          i ++ // skip alpha channel
          return {h, s: s/100, l: l/100}
        }))
      dispatch(updateBaseMatrix(baseMatrix))
      dispatch(refresh())
    })
  } else {
    dispatch(state => ({...state, baseMatrix: createBaseMatrix(state)}))
  }
}

export const updateStandardDeviation = (name, value) => state => {
  const { standardDeviations } = state
  return {...state, standardDeviations: {...standardDeviations, [name]: value}}
}

export const changeColor = hsl => state => (dispatch, _getState) => {
  dispatch(state => ({...state, base: hsl, imageUrl: null}))
  dispatch(refreshBaseMatrix())
}

export const updateLength = length => state => {
  return refresh()({...state, length})
}

export const updateWidth = width => state => (dispatch, _getState) => {
  dispatch(state => ({...state, width}))
  dispatch(refreshBaseMatrix())
  dispatch(refresh())
}

export const updateHeight = height => state => (dispatch, _getState) => {
  dispatch(state => ({...state, height}))
  dispatch(refreshBaseMatrix())
  dispatch(refresh())
}

export const updateBaseMatrix = baseMatrix => state => {
  return {...state, baseMatrix}
}

export const updateImageUrl = imageUrl => state => (dispatch, _getState) => {
  dispatch(state => ({...state, imageUrl}))
  dispatch(refreshBaseMatrix())
}

export const reset = () => () => (dispatch, _getState) => {
  const [h, s, l] = cc.hex.hsl('666696')
  dispatch(changeColor({h, s: s/100, l: l/100}))
  dispatch(updateStandardDeviation('h', 120))
  dispatch(updateStandardDeviation('s', 0))
  dispatch(updateStandardDeviation('l', 0))
  dispatch(updateWidth(4))
  dispatch(updateHeight(4))
  dispatch(updateLength(1))
  dispatch(refresh())
}
