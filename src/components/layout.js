import React from 'react'
import Helmet from 'react-helmet'

import '../layouts/index.css'
import '../layouts/app.css'

const Header = () => (
  <div
    style={{
      background: 'rebeccapurple',
      marginBottom: '1.45rem',
    }}
  >
  </div>
)

const TemplateWrapper = ({ children }) => (
  <div>
    <Helmet
      title="16icon"
      meta={[
        { name: 'description', content: 'The art of icons.' },
        { name: 'keywords', content: 'icon, art, colors' },
        { name: 'viewport', content: 'width=device-width, initial-scale=1.0' }
      ]}
    />
    <Header />
    <div
      style={{
        margin: '0 auto',
        maxWidth: 960,
        padding: '0px 1.0875rem 1.45rem',
        paddingTop: 0,
      }}
    >
      {children}
    </div>
  </div>
)

export default TemplateWrapper
