import React, { Component } from 'react'
import { connect } from 'react-redux'

import { reset } from '../reducers'

const mapDispatchToProps = {
  reset
}

class ResetButton extends Component {
  handleClick() {
    const { reset } = this.props
    reset()
  }
  render() {
    return <button className="button" onClick={::this.handleClick}>Reset Parameters</button>
  }
}

export default connect(null, mapDispatchToProps)(ResetButton)
