import React, { Component } from 'react'
import { connect } from 'react-redux'
import RCSlider from 'rc-slider'
import 'rc-slider/assets/index.css';

import { updateStandardDeviation } from '../reducers'

const mapStateToProps = ({ standardDeviations }) => {
  return {standardDeviations}
}
const mapDispatchToProps = {
  updateStandardDeviation
}

class Slider extends Component {
  render() {
    return (
      <div className="slider">
        <RCSlider {...this.props} />
      </div>
    )
  }
}

class Sliders extends Component {
  handleChangeH(value) {
    const { updateStandardDeviation } = this.props
    updateStandardDeviation('h', value)
  }
  handleChangeS(value) {
    const { updateStandardDeviation } = this.props
    updateStandardDeviation('s', value)
  }
  handleChangeL(value) {
    const { updateStandardDeviation } = this.props
    updateStandardDeviation('l', value)
  }
  render() {
    const {standardDeviations: { h, s, l }} = this.props
    // TODO Use `<></>` instead of `<div></div>`.
    return (
      <div>
        <Slider
          onChange={::this.handleChangeH}
          value={h} min={0} max={360}
        />
        <Slider
          onChange={::this.handleChangeS}
          value={s} min={0} max={100}
        />
        <Slider
          onChange={::this.handleChangeL}
          value={l} min={0} max={100}
        />
      </div>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Sliders)
